package com.example.pokemoninfocenter;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class PokemonModel {
    public PokemonModel(String name, String url){
        this.name = name;
        this.url = url;
    }

    public PokemonModel(String name){
        this.name = name;
    }

    private String name;
    private String url;

    public String getName(){return name;}

    public String getUrl(){ return url; }

    public List<Field> getAllPrivateMembers(){
        List<Field> privateFields = new ArrayList<>();
        Field[] allFields = this.getClass().getDeclaredFields();

        for(Field field: allFields){
            if(Modifier.isPrivate(field.getModifiers())){
                privateFields.add(field);
            }
        }

        return  privateFields;
    }

    public Field getPrivateMember(String privateMember){
        Field field = null;

        try{
            field = this.getClass().getDeclaredField(privateMember);
        }catch (NoSuchFieldException ex){
            field = null;
        }finally {
            return field;
        }
    }

    public Object getPrivateMember(String privateMember, Boolean printValue){
        Object value = null;

        Field field = this.getPrivateMember(privateMember);

        if(!printValue)
            return field;

        if(!field.equals(null)){
            if(!field.isAccessible())
                field.setAccessible(true);

            try{
                value = field.get(this);
            }catch(IllegalAccessException ex){

            }
            finally {
                if(field.isAccessible())
                    field.setAccessible(false);

                return value;
            }
        }

        return value;
    }
}
