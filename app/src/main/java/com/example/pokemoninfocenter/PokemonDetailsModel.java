package com.example.pokemoninfocenter;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class PokemonDetailsModel {

    public PokemonDetailsModel(int id, String name, int height, int weight){
        this.id = id;
        this.name = name;
        this.height = height;
        this.weight = weight;
    }

    private int id;
    private String name;
    private int height;
    private int weight;

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public int getHeight(){
        return height;
    }

    public int getWeight(){
        return weight;
    }

    public List<Field> getAllPrivateMembers(){
        List<Field> privateFields = new ArrayList<>();
        Field[] allFields = this.getClass().getDeclaredFields();

        for(Field field: allFields){
            if(Modifier.isPrivate(field.getModifiers())){
                privateFields.add(field);
            }
        }

        return  privateFields;
    }

    public Field getPrivateMember(String privateMember){
        Field field = null;

        try{
            field = this.getClass().getDeclaredField(privateMember);
        }catch (NoSuchFieldException ex){
            field = null;
        }finally {
            return field;
        }
    }

    public Object getPrivateMember(String privateMember, Boolean printValue){
        Object value = null;

        Field field = this.getPrivateMember(privateMember);

        if(!printValue)
            return field;

        if(!field.equals(null)){
            if(!field.isAccessible())
                field.setAccessible(true);

            try{
                value = field.get(this);
            }catch(IllegalAccessException ex){

            }
            finally {
                if(field.isAccessible())
                    field.setAccessible(false);

                return value;
            }
        }

        return value;
    }
}
