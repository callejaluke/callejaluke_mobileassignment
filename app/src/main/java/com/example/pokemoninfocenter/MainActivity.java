package com.example.pokemoninfocenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.firebase.auth.AuthResult;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView tvPokemonSearched;
    Button btnSearch;

    //API references
    RequestQueue queue;
    String apiBaseUrl = "https://pokeapi.co/api/v2/pokemon"; //returns all the pokemon

    //Recycler view references
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        tvPokemonSearched = (TextView)findViewById(R.id.txtPokemonNameSearch);
        btnSearch = (Button)findViewById(R.id.btnSearch);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initAPICall();
    }

    public void searchClick(View v){
        EditText pokemonToSearchFor = (EditText)findViewById(R.id.txtPokemonNameSearch);
        String pokemonToSearchText = pokemonToSearchFor.getText().toString();

        if(pokemonToSearchText.equals("")){ //== does not work with strings
            Toast.makeText(this, "Please enter valid text", Toast.LENGTH_LONG).show();
        }

        else{
            Intent intent = new Intent(getBaseContext(), PokemonDetailsActivity.class);
            intent.putExtra("Pokemon_Name", pokemonToSearchText); //pass the pokemon searched for name
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent iHomeIntent = new Intent(this, MainActivity.class);
            startActivity(iHomeIntent);
        } else if (id == R.id.nav_favourites) {
            Intent iFavouritesIntent = new Intent(this, FavouritesActivity.class);
            startActivity(iFavouritesIntent);
        } else if (id == R.id.nav_compare) {
            Intent iCompareIntent = new Intent(this, CompareActivity.class);
            startActivity(iCompareIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //On Start method
    @Override
    public void onStart(){
        super.onStart();

        showSnackbar();
    }

    //On Resume method
    @Override
    public void onResume(){
        super.onResume();

        showSnackbar();
    }

    //On Pause method
    @Override
    public void onPause(){
        super.onPause();

        saveDateTimeToSharedPreferences();
    }

    //on stop method
    @Override
    public void onStop(){
        super.onStop();

        saveDateTimeToSharedPreferences();
    }

    //method to show the snackbard to the user
    public void showSnackbar(){
        View view = (View)findViewById(R.id.rvPokemon); //current view

        long dateTimeLastActionLong = getDateTimeFromSharedPreferences(); //get the value from shared preferences
        Date dateTimeLastAction = new Date(dateTimeLastActionLong); //convert the long value to date and time

        Snackbar snackbar = Snackbar.make(view, "Date and Time of Last Action: " + dateTimeLastAction, Snackbar.LENGTH_LONG); //create the snackbar with the time
        snackbar.show(); //show the snackbar
    }

    //method to save the date and time to shared preferences
    public void saveDateTimeToSharedPreferences(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Preferences", 0); //the shared preferences being used
        SharedPreferences.Editor editor = pref.edit(); //the editor, to edit what is in the shared preferences

        Date date = new Date(System.currentTimeMillis()); //current date and time
        long millis = date.getTime(); //converted to long to be stored in the shared prefs
        editor.putLong("CURRENT_DATE_TIME", millis); //save to the editor

        editor.commit(); //commit the changes
    }

    //method to return the saved date and time from shared preferences
    public long getDateTimeFromSharedPreferences(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Preferences", 0); //initialise the shared prefs being used
        //SharedPreferences.Editor editor = pref.edit();

        return pref.getLong("CURRENT_DATE_TIME", 0L); //return the last date time saved in the shared prefs
    }

    //clicking the create pin button from the top right menu
    public void onCreatePinClick(MenuItem item){
        createPin();
    }

    //clicking the change pin button from the top right menu
    public void onChangePinClick(MenuItem item){
        changePin();
    }

    public void onRemovePinClick(MenuItem item){ removePin(); }

    //allow the user to create a pin number
    public void createPin(){
        //Pin Lock View
        Pinview pinview = (Pinview)findViewById(R.id.pinView);
        //show the pin lock view
        pinview.setVisibility(View.VISIBLE);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("PinValue", 0); //the shared preferences being used (for the pin)
                SharedPreferences.Editor editor = pref.edit();

                String pin = pinview.getValue();
                editor.putString("PIN_VALUE", pin);

                editor.commit();

                Toast.makeText(MainActivity.this, "Pin successfully created!", Toast.LENGTH_LONG).show();
                pinview.setVisibility(View.INVISIBLE); //hide the pin view after being done with it
                clearPin(); //clear the pin numbers when ready
            }
        });
    }

    //allow the user to change their pin
    public void changePin(){
        //Pin Lock View
        Pinview pinview = (Pinview)findViewById(R.id.pinView);
        //show the pin lock view
        pinview.setVisibility(View.VISIBLE);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {

                SharedPreferences pref = getApplicationContext().getSharedPreferences("PinValue", 0); //the shared preferences being used (for the pin)
                SharedPreferences.Editor editor = pref.edit();

                String pin = pinview.getValue();
                editor.putString("PIN_VALUE", pin);

                editor.commit();

                Toast.makeText(MainActivity.this, "Pin successfully changed!", Toast.LENGTH_LONG).show();
                pinview.setVisibility(View.INVISIBLE); //hide the pin view after being done with it
                clearPin(); //clear the pin numbers when ready
            }
        });
    }

    //remove the pin completely
    public void removePin(){

        SharedPreferences pref = getApplicationContext().getSharedPreferences("PinValue", 0); //the shared preferences being used (for the pin)
        SharedPreferences.Editor editor = pref.edit();

        editor.clear();
        editor.commit();

        Toast.makeText(MainActivity.this, "Pin successfully removed!", Toast.LENGTH_LONG).show();
    }

    //method to clear the pin view text when done with it
    public void clearPin(){
        Pinview pinview = (Pinview)findViewById(R.id.pinView);
        for(int i = 0; i < pinview.getChildCount(); i++){
            EditText child = (EditText)pinview.getChildAt(i);
            child.setText("");
        }
    }

    private void initRecyclerView(List<PokemonModel> pokemonDataStore){
        //Find the recycler view
        recyclerView = (RecyclerView)findViewById(R.id.rvPokemon);

        //content does not change the layout of the view
        recyclerView.setHasFixedSize(true);

        //using linear layout manager
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);

        //divide items to easily distinguish between one pokemon and another
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        //specify an adapter
        mAdapter = new Adapter<PokemonModel>(this, "recycleritemtemplate", pokemonDataStore){

            @Override
            public void configRecyclerItemModel(Item itemContext, View itemView){
                //override this method to configure all properties that each recycler item model should hold

                itemContext.textView = (TextView)itemView.findViewById(R.id.recyclerItem_Label);

                itemContext.linearLayout = (LinearLayout)itemView.findViewById(R.id.recyclerContainer);
            }

            @Override
            public void paintRecyclerItem(RecyclerView.ViewHolder holder, final int position, PokemonModel dataObject){
                //override this method to reflect the data to be shown in the recycler view items

                String result = /*"Name: " +*/ dataObject.getName(); //+ " | ";
                //result = result + "URL: " + dataObject.getUrl();

                ((Item) holder).textView.setText(result);
            }

            @Override
            public void onRecyclerItemClick(View v, final RecyclerView.ViewHolder holder, final int position, PokemonModel dataObject){
                //override this method to handle on click action on recycler view items (this will allow the user to redirect to the individual pokemon page)

                Intent dataIntent = new Intent();
                dataIntent.setClass(v.getContext(), PokemonDetailsActivity.class);

                dataIntent.putExtra("Pokemon_Name", dataObject.getName()); //pass the name to the next intent

                v.getContext().startActivity(dataIntent);
            }
        };

        // set adapter to recycler view
        recyclerView.setAdapter(mAdapter);
    }

    //call the API
    private void initAPICall(){
        queue = Volley.newRequestQueue(getApplicationContext());

        String url = apiBaseUrl;

        //execute call to the PokeAPI

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //On request successfull

                        Log.i("MainActivity", response.toString());

                        try {
                            //retrieving the JSON result

                            //accessing all the pokemon given
                            JSONArray pokemonList = response.getJSONArray("results");

                            JSONObject pokemonObj;

                            JSONObject pokemon_mainObj;

                            String _name, _url;

                            List<PokemonModel> _pokemonDataStore = new ArrayList<>();

                            for (int i = 0; i < pokemonList.length(); i++) {
                                //accessing each JSON object within the pokemon list
                                pokemonObj = pokemonList.getJSONObject(i);

                                //Accessing a particular property NOT NEEDED
                                //pokemon_mainObj = pokemonObj.getJSONObject("results");

                                //accessing the 2 main properties - name and url
                                _name = pokemonObj.getString("name");
                                _url = pokemonObj.getString("url");

                                //creating a pokemon model instance to model each data context from above
                                PokemonModel _pokemonModel = new PokemonModel(_name, _url);

                                //adding the created pokemon model to the view
                                _pokemonDataStore.add(_pokemonModel);

                            }

                            initRecyclerView(_pokemonDataStore);

                        } catch (JSONException ex) {
                            Log.i("MainActivity", ex.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //request failed
                Log.i("MainActivity", error.toString());
            }
        });

        //add the request to the requestQueue
        queue.add(stringRequest);
    }

}
