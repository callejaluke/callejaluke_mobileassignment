package com.example.pokemoninfocenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class PokemonDetailsActivity extends AppCompatActivity {

    String apiBaseUrl = "https://pokeapi.co/api/v2/pokemon/";
    String pokemonName = "";
    String fullApiUrl = "";

    //API references
    RequestQueue queue;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_details);

        Intent pokemonIntent = getIntent();

        Bundle bundle = pokemonIntent.getExtras();

        if(bundle != null){
            pokemonName = bundle.getString("Pokemon_Name");
            pokemonName = pokemonName.toLowerCase(); //change to lower case, otherwise the API wont return the data
            fullApiUrl = apiBaseUrl + pokemonName;

            initAPICall();
        }

        Button btnAddToFavourites = findViewById(R.id.btnAddToFavourites);
        Button btnSeeMore = findViewById(R.id.btnSeeMore);
    }

    //add the pokemon to the favourites shared pref (only add to favourites after applying changes) in the favourites screen
    public void addToFavouritesClick(View v){
        saveToFavourites();

        //Toast.makeText(this, "Pokemon added to favourites. Remember to apply changes from the Favourites page", Toast.LENGTH_LONG).show();
    }

    //On Pause method
    @Override
    public void onPause(){
        super.onPause();

        saveToFavourites();
    }

    //on stop method
    @Override
    public void onStop(){
        super.onStop();

        saveToFavourites();
    }

    public void seeMoreClick(View v){
        String url = "https://bulbapedia.bulbagarden.net/wiki/" + pokemonName; //the url to redirect to implicit intents

        Intent morePkmnDetailsIntent = new Intent(Intent.ACTION_VIEW);
        morePkmnDetailsIntent.setData(Uri.parse(url));

        startActivity(morePkmnDetailsIntent);
    }

    public void saveToFavourites(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Favourites", 0); //get the shared preference
        SharedPreferences.Editor editor = pref.edit(); //get the editor

        String favouriteList = pref.getString("Favourites", "DEFAULT");

        if(!favouriteList.contains(pokemonName)){ //if the pokemon is not already saved in the favourites
            if(favouriteList != "DEFAULT"){ //there are already items in the list, append
                favouriteList = favouriteList + "," + pokemonName; //add the pokemon to the current list
            }
            else{ //there are no items in the list, add
                favouriteList = pokemonName; //the new pokemon is currently the only one in the list
            }

            editor.putString("Favourites", favouriteList);
            editor.commit();

            Toast.makeText(this, "Pokemon added to favourites. Remember to apply changes from the Favourites page", Toast.LENGTH_LONG).show();
        }

        else{ //pokemon already saved as favourite
            //Toast.makeText(this, "This Pokemon is already in your favourites", Toast.LENGTH_LONG).show();
        }
    }

    private void initRecyclerView(List<PokemonDetailsModel> pokemonDetailsStore){
        //Find the recycler view
        recyclerView = (RecyclerView)findViewById(R.id.rvPokemonDetails);

        //content does not change the layout of the view
        recyclerView.setHasFixedSize(true);

        //using linear layout manager
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);

        //divide items to easily distinguish between one pokemon and another
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        //specify an adapter
        mAdapter = new Adapter<PokemonDetailsModel>(this, "recycleritemtemplate", pokemonDetailsStore){

            @Override
            public void configRecyclerItemModel(Item itemContext, View itemView){
                //override this method to configure all properties that each recycler item model should hold

                itemContext.textView = (TextView)itemView.findViewById(R.id.recyclerItem_Label);

                itemContext.linearLayout = (LinearLayout)itemView.findViewById(R.id.recyclerContainer);
            }

            @Override
            public void paintRecyclerItem(RecyclerView.ViewHolder holder, final int position, PokemonDetailsModel dataObject){
                //override this method to reflect the data to be shown in the recycler view items

                String result = "ID: " + dataObject.getId() + " | ";
                result = result + "Name: " + dataObject.getName() + " | ";
                result = result + "Height: " + dataObject.getHeight() + " inches | ";
                result = result + "Weight: " + dataObject.getWeight() + " grams";

                ((Item) holder).textView.setText(result);
            }
        };

        // set adapter to recycler view
        recyclerView.setAdapter(mAdapter);

    }

    //call the API
    private void initAPICall(){
        queue = Volley.newRequestQueue(getApplicationContext());

        String url = fullApiUrl;

        //execute call to the PokeAPI

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //On request successful

                        Log.i("DetailsActivity", response.toString());

                        try {
                            //retrieving the JSON result/object

                            JSONObject pokemonObj = response;

                            int _id, _height, _weight;
                            String _name;

                            List<PokemonDetailsModel> _pokemonDetailsStore = new ArrayList<>();

                            //accessing the properties required
                            _id = pokemonObj.getInt("id");
                            _name = pokemonObj.getString("name");
                            _height = pokemonObj.getInt("height");
                            _weight = pokemonObj.getInt("weight");

                            //creating a pokemon model instance to model each data context from above
                            PokemonDetailsModel _pokemonDetailsModel = new PokemonDetailsModel(_id, _name, _height, _weight);

                            //adding the created pokemon model to the view
                            _pokemonDetailsStore.add(_pokemonDetailsModel);

                            initRecyclerView(_pokemonDetailsStore);

                        } catch (JSONException ex) {
                            Log.i("MainActivity", ex.toString());
                            Toast.makeText(getApplicationContext(), "Unable to get Pokemon details. Please make sure name is spelt correctly in search", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //request failed
                Log.i("DetailsActivity", error.toString());
                Toast.makeText(getApplicationContext(), "Unable to get Pokemon details. Please make sure name is spelt correctly in search", Toast.LENGTH_LONG).show();
            }
        });

        //add the request to the requestQueue
        queue.add(stringRequest);
    }
}
