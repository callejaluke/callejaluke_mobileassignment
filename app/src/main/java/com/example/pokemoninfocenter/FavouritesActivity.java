package com.example.pokemoninfocenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class FavouritesActivity extends AppCompatActivity {

    boolean pinCorrect;

    //Recycler view references
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
    }

    //On Start method
    @Override
    public void onStart(){
        super.onStart();

        try {
            boolean pinCreated = checkIfPinCreated();

            if(pinCreated){
                Button enterPinButton = (Button)findViewById(R.id.btnEnterPin);
                enterPinButton.setVisibility(View.VISIBLE);
            }

            else{
                showFavourites();
            }

        }catch (Exception ex){
            Toast.makeText(this, "Error: " + ex.toString(), Toast.LENGTH_LONG).show();
        }
    }

    //On Resume method
    @Override
    public void onResume(){
        super.onResume();

        try {
            boolean pinCreated = checkIfPinCreated();

            if(pinCreated){
                Button enterPinButton = (Button)findViewById(R.id.btnEnterPin);
                enterPinButton.setVisibility(View.VISIBLE);
            }

            else{
                showFavourites();
            }

        }catch (Exception ex){
            Toast.makeText(this, "Error: " + ex.toString(), Toast.LENGTH_LONG).show();
        }
    }

    //method to check whether a pin has been created or not
    public boolean checkIfPinCreated(){
        boolean pinCreated = false;

        SharedPreferences pref = getApplicationContext().getSharedPreferences("PinValue", 0); //the shared preferences being used (for the pin)

        String pinSavedInMemory = pref.getString("PIN_VALUE", "DEFAULT");

        if(pinSavedInMemory != "DEFAULT"){ //if the default value is not returned, then a pin has been created
            pinCreated = true;
        }

        return pinCreated;
    }

    //method to clear the pin view when done with it
    public void clearPin(){
        Pinview pinview = (Pinview)findViewById(R.id.pinViewFavourites);
        for(int i = 0; i < pinview.getChildCount(); i++){
            EditText child = (EditText)pinview.getChildAt(i);
            child.setText("");
        }
    }

    public void btnEnterPinClicked(View view){
        Pinview pinview = (Pinview)findViewById(R.id.pinViewFavourites);
        pinview.setVisibility(View.VISIBLE);

        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) { //upon entering all the digits required for the pin

                SharedPreferences pref = getApplicationContext().getSharedPreferences("PinValue", 0); //the shared preferences being used (for the pin)

                String pinEnteredByUser = pinview.getValue();
                String pinSavedInMemory = pref.getString("PIN_VALUE", "DEFAULT");

                if(pinEnteredByUser.equals(pinSavedInMemory)){ //== does not work with strings
                    pinCorrect = true;
                    showFavourites();
                }

                else{
                    pinCorrect = false;
                    showIncorrectPinToast();
                }

                pinview.setVisibility(View.INVISIBLE); //hide the pin view after being done with it
                clearPin(); //clear the pin numbers when ready
            }
        });

    }

    public void showIncorrectPinToast(){
        Toast.makeText(this, "Error: Incorrect PIN", Toast.LENGTH_LONG).show();
    }

    public void btnSaveFavouritesClicked(View view){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Favourites", 0);
        String favouriteList = pref.getString("Favourites", "DEFAULT");

        if(favouriteList != "DEFAULT"){
            String[] favouritesArray = favouriteList.split(",");

            //FirebaseDatabase dbFavourites = FirebaseDatabase.getInstance();
            //DatabaseReference dbFavouritesRef = FirebaseDatabase.getInstance().getReference("favourites");

            //dbFavouritesRef.setValue(favouritesArray);
        }

        else{ //no pokemon saved in shared pref
            Toast.makeText(this, "No favourites to commit", Toast.LENGTH_LONG).show();
        }
    }

    //method to show the favourites if the user has entered the correct pin, or if the user has no pin set
    public void showFavourites(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Favourites", 0);
        String favouriteList = pref.getString("Favourites", "DEFAULT");

        if(favouriteList != "DEFAULT"){
            String[] favouritesArray = favouriteList.split(",");

            String _name;
            List<PokemonModel> _pokemonFavouritesStore = new ArrayList<>();

            for(int i = 0; i < favouritesArray.length; i++){
                _name = favouritesArray[i];

                PokemonModel _pokemonModel = new PokemonModel(_name);
                _pokemonFavouritesStore.add(_pokemonModel);
            }

            initRecyclerView(_pokemonFavouritesStore);
        }

        else{ //no pokemon saved as favourites
            Toast.makeText(this, "No favourites to show", Toast.LENGTH_LONG).show();
        }
    }

    private void initRecyclerView(List<PokemonModel> pokemonFavouritesStore){
        //Find the recycler view
        recyclerView = (RecyclerView)findViewById(R.id.rvFavourites);

        //content does not change the layout of the view
        recyclerView.setHasFixedSize(true);

        //using linear layout manager
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);

        //divide items to easily distinguish between one pokemon and another
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        //specify an adapter
        mAdapter = new Adapter<PokemonModel>(this, "recycleritemtemplate", pokemonFavouritesStore){

            @Override
            public void configRecyclerItemModel(Item itemContext, View itemView){
                //override this method to configure all properties that each recycler item model should hold

                itemContext.textView = (TextView)itemView.findViewById(R.id.recyclerItem_Label);

                itemContext.linearLayout = (LinearLayout)itemView.findViewById(R.id.recyclerContainer);
            }

            @Override
            public void paintRecyclerItem(RecyclerView.ViewHolder holder, final int position, PokemonModel dataObject){
                //override this method to reflect the data to be shown in the recycler view items

                String result = /*"Name: " +*/ dataObject.getName(); //+ " | ";
                //result = result + "URL: " + dataObject.getUrl();

                ((Item) holder).textView.setText(result);
            }

            @Override
            public void onRecyclerItemClick(View v, final RecyclerView.ViewHolder holder, final int position, PokemonModel dataObject){
                //override this method to handle on click action on recycler view items (this will allow the user to redirect to the individual pokemon page)

                Intent dataIntent = new Intent();
                dataIntent.setClass(v.getContext(), PokemonDetailsActivity.class);

                dataIntent.putExtra("Pokemon_Name", dataObject.getName()); //pass the name to the next intent

                v.getContext().startActivity(dataIntent);
            }
        };

        // set adapter to recycler view
        recyclerView.setAdapter(mAdapter);
    }

}
